<?php
/**
 * Enqueue scripts and styles.
 */
function arif_scripts()
{

//    all style
    wp_enqueue_style('bgl_group-bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), _S_VERSION);
    wp_enqueue_style('bgl_group-slick', get_template_directory_uri() . '/assets/css/slick.min.css', array(), _S_VERSION);
    wp_enqueue_style('bgl_group-layer-slider', get_template_directory_uri() . '/assets/css/layerslider.css', array(), _S_VERSION);
    wp_enqueue_style('bgl_group-light-gallery', get_template_directory_uri() . '/assets/css/lightgallery.css', array(), _S_VERSION);
    wp_enqueue_style('bgl_group-dc-animation', get_template_directory_uri() . '/assets/css/dc-animation.css', array(), _S_VERSION);
    wp_enqueue_style('bgl_group-theme-style', get_template_directory_uri() . '/assets/css/home.css', array(), _S_VERSION);
    wp_enqueue_style('bgl_group-style', get_stylesheet_uri(), array(), _S_VERSION);
    wp_style_add_data('bgl_group-style', 'rtl', 'replace');


//    all javascript file
    wp_enqueue_script('bgl_group-js-boostrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), _S_VERSION, true);
    wp_enqueue_script('bgl_group-js-greensock', get_template_directory_uri() . '/assets/js/greensock.js', array('jquery'), _S_VERSION, true);
    wp_enqueue_script('bgl_group-js-layer-slider', get_template_directory_uri() . '/assets/js/layerslider.transitions.js', array('jquery'), _S_VERSION, true);
    wp_enqueue_script('bgl_group-js-kreaturamedia', get_template_directory_uri() . '/assets/js/layerslider.kreaturamedia.jquery.js', array('jquery'), _S_VERSION, true);
    wp_enqueue_script('bgl_group-js-gsap-min-updated', get_template_directory_uri() . '/assets/js/gsap.min.js', array('jquery'), _S_VERSION, true);
    wp_enqueue_script('bgl_group-js-ScrollTrigger', get_template_directory_uri() . '/assets/js/ScrollTrigger.min.js', array('jquery'), _S_VERSION, true);
    wp_enqueue_script('bgl_group-js-tweenmax', get_template_directory_uri() . '/assets/js/tweenmax.js', array('jquery'), _S_VERSION, true);
    wp_enqueue_script('bgl_group-js-css-rules', get_template_directory_uri() . '/assets/js/css-rules.min.js', array('jquery'), _S_VERSION, true);
    wp_enqueue_script('bgl_group-js-counter', get_template_directory_uri() . '/assets/js/counter.js', array('jquery'), _S_VERSION, true);
    wp_enqueue_script('bgl_group-js-diurnal', get_template_directory_uri() . '/assets/js/jquery.diurnal.min.js', array('jquery'), _S_VERSION, true);
    wp_enqueue_script('bgl_group-js-nice-select', get_template_directory_uri() . '/assets/js/jquery.nice-select.min.js', array('jquery'), _S_VERSION, true);
    wp_enqueue_script('bgl_group-js-slick', get_template_directory_uri() . '/assets/js/slick.js', array('jquery'), _S_VERSION, true);
    wp_enqueue_script('bgl_group-js-image', get_template_directory_uri() . '/assets/js/image-preloader.js', array('jquery'), _S_VERSION, true);
    wp_enqueue_script('bgl_group-js-dc-animation', get_template_directory_uri() . '/assets/js/dc-animation.js', array('jquery'), _S_VERSION, true);
    wp_enqueue_script('bgl_group-js-global', get_template_directory_uri() . '/assets/js/global.js', array('jquery'), _S_VERSION, true);
    wp_enqueue_script('bgl_group-js-home', get_template_directory_uri() . '/assets/js/home.js', array('jquery'), _S_VERSION, true);
    wp_enqueue_style('bgl_group-theme-style', get_template_directory_uri() . '/assets/css/style.css', array(), _S_VERSION);
    wp_enqueue_script('bgl_group-navigation', get_template_directory_uri() . '/js/navigation.js', array('jquery'), _S_VERSION, true);
    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'arif_scripts');


