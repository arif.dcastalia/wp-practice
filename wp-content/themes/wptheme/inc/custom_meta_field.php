<?php

add_action('cmb2_admin_init', 'cmb2_sample_metaboxes');

function cmb2_sample_metaboxes()
{
    $form_section_aus = new_cmb2_box(array(
        'id' => 'custom_post',
        'title' => 'Custom Post Metabox',
        'context' => 'normal',
        'object_types' => array('custom_post'),
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'closed' => true,
    ));
    $form_section_aus->add_field(array(
        'name' => 'Custom Text',
        'id' => 'custom_text',
        'type' => 'text',
    ));
    $form_section_aus->add_field(array(
        'name' => 'Custom File',
        'id' => 'custom_file',
        'type' => 'file',
    ));

    // isotope  custom field
    $news_event = new_cmb2_box(array(
        'id' => 'isotope',
        'title' => 'isotope Metabox',
        'context' => 'normal',
        'object_types' => array('isotope'), // Post type
//        'show_on' => array('key' => 'page-template', 'value' => 'template-parts/template-home-aus.php'),
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'closed' => true,
    ));
    $news_event->add_field(array(
        'name' => 'Thumb Background',
        'id' => 'isotope_image',
        'type' => 'file',
    ));
// cmb2 Banner
    $home_slider = new_cmb2_box(array(
        'id' => 'home_page',
        'title' => 'Banner',
        'context' => 'normal',
        'object_types' => array('page'), // Post type
        'show_on' => array('key' => 'page-template', 'value' => 'template-cmb2.php'),
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'closed' => true,
    ));
    $home_slider->add_field(array(
        'name' => 'Thumb Background',
        'id' => 'banner_image',
        'type' => 'file',
    ));

    $home_slider->add_field(array(
        'name' => 'Title',
        'id' => 'title',
        'type' => 'text',
    ));

    $home_slider->add_field(array(
        'name' => 'Subtitle',
        'id' => 'sub_title',
        'type' => 'text',
    ));

    $home_slider->add_field(array(
        'name' => 'Shortdes',
        'id' => 'short_des',
        'type' => 'text',
    ));
    
    //About us section cm2
    
    $about_us = new_cmb2_box(array(
        'id' => 'About us',
        'title' => 'About us ',
        'context' => 'normal',
        'object_types' => array('page'), // Post type
        'show_on' => array('key' => 'page-template', 'value' => 'template-cmb2.php'),
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'closed' => true,
    ));
    $about_us->add_field(array(
        'name' => 'Email Image',
        'id' => 'about_us_image',
        'type' => 'file',
    ));

    $about_us->add_field(array(
        'name' => 'About Heading',
        'id' => 'heading_text',
        'type' => 'textarea',
    ));

    $about_us->add_field(array(
        'name' => 'left text',
        'id' => 'left_text',
        'type' => 'textarea',
    ));

    $about_us->add_field(array(
        'name' => 'Middle Text',
        'id' => 'middle_text',
        'type' => 'textarea',
    ));

    $about_us->add_field(array(
        'name' => 'Right Text',
        'id' => 'right_text',
        'type' => 'textarea',
    ));

    // Details page CMB2

    $details_cmb2 = new_cmb2_box(array(
        'id' => 'details_cmb2',
        'title' => 'Detail Page',
        'context' => 'normal',
        'object_types' => array('page'), // Post type
        'show_on' => array('key' => 'page-template', 'value' => 'template-cmb2.php'),
        'priority' => 'high',
        'show_names' => true, // Show field names on the left
        'closed' => true,
    ));
    $details_cmb2->add_field(array(
        'name' => 'Thumb Background',
        'id' => 'details_Background_img',
        'type' => 'file',
    ));

    $details_cmb2->add_field(array(
        'name' => 'Title',
        'id' => 'details_title',
        'type' => 'text',
    ));

    $details_cmb2->add_field(array(
        'name' => 'Table',
        'id' => 'details_table',
        'type' => 'wysiwyg',
        'sanitization_cb' => false,
    ));

}








