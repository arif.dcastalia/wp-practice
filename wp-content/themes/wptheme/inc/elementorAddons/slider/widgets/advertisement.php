<?php

namespace WPC\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly


class Slider extends Widget_Base
{

    public function get_name()
    {
        return 'slider';
    }

    public function get_title()
    {
        return 'slider (arif)';
    }

    public function get_icon()
    {
        return 'eicon-slider-device';
    }

    public function get_categories()
    {
        return ['bgl_category'];
    }

    protected function register_controls()
    {

        $this->start_controls_section(
            'section_content',
            [
                'label' => 'Settings',
            ]
        );

        $this->add_control(
            'heading',
            [
                'label' => 'Label Heading',
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => 'Features and Amenities'
            ]
        );

        // $this->add_control(
        //     'active_link',
        //     [
        //         'label' => esc_html__('Active Clickable', 'elementor'),
        //         'type' => Controls_Manager::SELECT,
        //         'options' => [
        //             '1' => 'Yes',
        //             '2' => 'No',
        //             'h3' => 'H3',
        //             'h4' => 'H4',
        //             'h5' => 'H5',
        //             'h6' => 'H6',
        //             'div' => 'div',
        //         ],
        //         'default' => '2',
        //     ]
        // );

        $this->end_controls_section();
    }


    protected function render()
    {
        $settings = $this->get_settings_for_display();

        $this->add_inline_editing_attributes('label_heading', 'basic');
        $this->add_render_attribute(
            'label_heading',
            [
                'class' => ['advertisement__label-heading'],
            ]
        );
        $settings = $this->get_settings_for_display();

        $args = array(
            'posts_per_page' => $settings['slide_to_show'] ? $settings['slide_to_show'] : 10,
            'order' => 'ASC',
            'orderby' => 'menu_order',
            'post_type' => 'slider',
            'order' => 'ASC'
        );
        $myposts = get_posts($args);
?>
<script>
    jQuery(document).ready(function ($) {
        var container_left = jQuery('.container').offset().left;
        $('.<?php echo $this->get_name() . $this->get_id(); ?>').not('.slick-initialized').slick({
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 2000,
            speed: 1000,
            prevArrow: $('#sustain_prev'),
            nextArrow: $('#sustain_next'),
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });


        if ($('.sustainability').length > 0) {
            $('.sustainability .slider_wrapper').css('margin-left', container_left + 15);

        }

        jQuery(window).on('resize', function () {
            var container_left = jQuery('.container').offset().left;

            $('.sustainability .slider_wrapper').css('margin-left', container_left + 15);

        });
    });
</script>
<section id="features" class="slider">

    <div class="container">

        <div class="slide-number">
            0 / 0
        </div>

        <div class="swiper">
            <div class="slider-init">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="row">
                            <div class="slider__left-content col-sm-5">
                                <h3>Features and Amenities</h3>
                                <div class="anim-active fade-up">
                                    <h2>Main Apartment</h2>
                                    <ul>
                                        <li><strong>Floor</strong> - Floors are imported 24" x 24" mirror polish
                                            homogeneous tiles.</li>
                                        <li><strong>Painting</strong> - Smooth finish plastic paint on all walls and
                                            ceiling in soft colors (Berger/Asian paint).</li>
                                        <li><strong>Main Door</strong> - Solid teak decorative main entrance door with
                                            door chain, check viewer, door knocker &amp; apartment number plate</li>
                                        <li><strong>Internal Door</strong> - Internal doors of strong and durable veneer
                                            flush door (Teak chamble) shutter and solid chowkath (Teak chamble) with
                                            French polish.</li>
                                        <li><strong>Window</strong> - Sliding windows with tinted/clear glass complete
                                            with mohair lining and rain water barrier in 4" aluminum sections. Safety
                                            grills in windows.</li>
                                        <li><strong>Electrical Features</strong> - MK electrical switches &amp; plug
                                            points. Electrical distribution box with circuit breaker. All power outlets.
                                        </li>
                                        <li><strong>Cable Connection</strong> - Cable TV line provision in one bedroom
                                            and living room and family living.</li>
                                        <li><strong>Phone Connection</strong> - Two telephone connection points in
                                            master
                                            bedroom and living area.</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="slider__right-content col-sm-5 offset-sm-2">
                                <div class="slider__right-content__inner">
                                    <img src="assets/images/dynamic/slider/1.jpg" alt="">
                                    <h3>Apartment</h3>
                                </div>
                            </div>
                        </div>

                        <li class="go-left">
                            <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 16 16"
                                height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M11.354 1.646a.5.5 0 010 .708L5.707 8l5.647 5.646a.5.5 0 01-.708.708l-6-6a.5.5 0 010-.708l6-6a.5.5 0 01.708 0z"
                                    clip-rule="evenodd">
                                </path>
                            </svg>
                        </li>

                        <li class="go-right"><svg stroke="currentColor" fill="currentColor" stroke-width="0"
                                viewBox="0 0 16 16" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M4.646 1.646a.5.5 0 01.708 0l6 6a.5.5 0 010 .708l-6 6a.5.5 0 01-.708-.708L10.293 8 4.646 2.354a.5.5 0 010-.708z"
                                    clip-rule="evenodd"></path>
                            </svg>
                        </li>

                    </div>

                    <div class="swiper-slide">

                        <div class="row">
                            <div class="slider__left-content col-sm-5">
                                <h3>Features and Amenities</h3>
                                <div class="anim-active fade-up">
                                    <h2>Kitchen</h2>
                                    <!-- <div class="slide-number">
                           0 / 0
                        </div> -->
                                    <ul>
                                        <li><strong>Platform</strong> - Impressively designed platform with
                                            marble/granite worktop.</li>
                                        <li><strong>Burner</strong> - Double burner gas outlet.</li>
                                        <li><strong>Wall</strong> - Glazed ceramic tiles
                                            (CHINESE/RAK/FUWANG/Equivalent) up-to ceiling.</li>
                                        <li><strong>Floor</strong> - Floors are imported 24" x 24" mirror polish
                                            homogeneous tiles.</li>
                                        <li><strong>Water Line</strong> - Hot and cold water line.</li>
                                        <li><strong>Sink</strong> - One stainless counter-top steel sink with
                                            mixer.</li>
                                        <li><strong>Washing Area</strong> - Washing area covered with tiles.
                                        </li>
                                        <li><strong>Exhaust Fan</strong> - Suitably located exhaust fan.</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="slider__right-content col-sm-5 offset-sm-2">
                                <div class="slider__right-content__inner">
                                    <img src="https://nhlbd.org/dev/admin/uploads/product/nddl-imams-heritage/1635748549ffheX.jpg"
                                        alt="">
                                    <h3>Kitchen</h3>
                                </div>
                            </div>
                        </div>

                        <li class="go-left">
                            <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 16 16"
                                height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M11.354 1.646a.5.5 0 010 .708L5.707 8l5.647 5.646a.5.5 0 01-.708.708l-6-6a.5.5 0 010-.708l6-6a.5.5 0 01.708 0z"
                                    clip-rule="evenodd">
                                </path>
                            </svg>
                        </li>

                        <li class="go-right"><svg stroke="currentColor" fill="currentColor" stroke-width="0"
                                viewBox="0 0 16 16" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M4.646 1.646a.5.5 0 01.708 0l6 6a.5.5 0 010 .708l-6 6a.5.5 0 01-.708-.708L10.293 8 4.646 2.354a.5.5 0 010-.708z"
                                    clip-rule="evenodd"></path>
                            </svg>
                        </li>

                    </div>

                    <div class="swiper-slide">
                        <!-- <div class="slide-number">
                     0 / 0
               </div>   -->
                        <div class="row">
                            <div class="slider__left-content col-sm-5">
                                <h3>Features and Amenities</h3>
                                <div class="anim-active fade-up">
                                    <h2>Bathroom</h2>
                                    <!-- <div class="slide-number">
                           0 / 0
                        </div> -->
                                    <ul>
                                        <li><strong>Door</strong> - All bathrooms with inner-side laminated
                                            veneered flash door.</li>
                                        <li><strong>Sanitary Wares</strong> - Good quality sanitary wares in all
                                            bathrooms (COTTO / CHINESE /Equivalent)</li>
                                        <li><strong>Bathroom Fitting</strong> - Good quality chrome plated
                                            fittings (GROHE/ CHINESE).</li>
                                        <li><strong>Wall</strong> - All bathroom walls will be glazed ceramic
                                            tiles (CHINESE/RAK&nbsp; /Equivalent) up to 7” height.</li>
                                        <li><strong>Floor</strong> - Matching ceramic floor tiles
                                            (CHINESE/RAK/FUWANG/Equivalent)</li>
                                        <li><strong>Bathtub</strong> - One imported bathtub in master bathroom.
                                        </li>
                                        <li><strong>Geyser</strong> - Geyser system in all bathrooms except
                                            servant’s toilet.</li>
                                        <li><strong>Basin</strong> - Cabinet basin in master bathroom and child
                                            bathroom.</li>
                                        <li><strong>Mirror</strong> - All mirrors in bathrooms with overhead
                                            lamps.</li>
                                        <li><strong>Maid’s Bath</strong> - RAK or equivalent floor and wall
                                            tiles up to 7” with long pan, shower and Low down.</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="slider__right-content col-sm-5 offset-sm-2">
                                <div class="slider__right-content__inner">
                                    <img src="https://nhlbd.org/dev/admin/uploads/product/nddl-imams-heritage/1635748571RjG7s.jpg"
                                        alt="">
                                    alt="">
                                    <h3>Bathroom</h3>
                                </div>
                            </div>
                        </div>

                        <li class="go-left">
                            <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 16 16"
                                height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M11.354 1.646a.5.5 0 010 .708L5.707 8l5.647 5.646a.5.5 0 01-.708.708l-6-6a.5.5 0 010-.708l6-6a.5.5 0 01.708 0z"
                                    clip-rule="evenodd">
                                </path>
                            </svg>
                        </li>

                        <li class="go-right"><svg stroke="currentColor" fill="currentColor" stroke-width="0"
                                viewBox="0 0 16 16" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd"
                                    d="M4.646 1.646a.5.5 0 01.708 0l6 6a.5.5 0 010 .708l-6 6a.5.5 0 01-.708-.708L10.293 8 4.646 2.354a.5.5 0 010-.708z"
                                    clip-rule="evenodd"></path>
                            </svg>
                        </li>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

<?php
    }

}