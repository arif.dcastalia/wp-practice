<?php

namespace WPC\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if (!defined('ABSPATH')) exit; // Exit if accessed directly


class LayerSliderWidget extends Widget_Base
{

    public function get_name()
    {
        return 'layersliderwidget';
    }

    public function get_title()
    {
        return 'Layer Slider (Image)';
    }

    public function get_icon()
    {
        return 'eicon-gallery-grid';
    }

    public function get_categories()
    {
        return ['bgl_category'];
    }

    protected function register_controls()
    {

        $this->start_controls_section(
            'section_content',
            [
                'label' => 'Settings',
            ]
        );


        /* Start repeater */

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'text',
            [
                'label' => esc_html__('Enter Title', 'elementor-list-widget'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'placeholder' => esc_html__('List Item', 'elementor-list-widget'),
                'default' => esc_html__('List Item', 'elementor-list-widget'),
                'label_block' => true,
                'dynamic' => [
                    'active' => true,
                ],
            ]
        );
        $repeater->add_control(
            'subtitle',
            [
                'label' => esc_html__('Enter Subtitle', 'elementor-list-widget'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'placeholder' => esc_html__('List Item', 'elementor-list-widget'),
                'default' => esc_html__('List Item', 'elementor-list-widget'),
                'label_block' => true,
                'dynamic' => [
                    'active' => true,
                ],
            ]
        );

        $repeater->add_control(
            'image',
            [
                'label' => esc_html__('Choose Image', 'textdomain'),
                'type' => \Elementor\Controls_Manager::MEDIA,
                'default' => [
                    'url' => \Elementor\Utils::get_placeholder_image_src(),
                ],
            ]
        );


        $this->add_control(
            'list_items',
            [
                'label' => esc_html__('List Items', 'elementor-list-widget'),
                'type' => \Elementor\Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),           /* Use our repeater */
                'default' => [
                    [
                        'text' => esc_html__('List Item #1', 'elementor-list-widget'),
                        'link' => '',
                    ],
                    [
                        'text' => esc_html__('List Item #2', 'elementor-list-widget'),
                        'link' => '',
                    ],
                    [
                        'text' => esc_html__('List Item #3', 'elementor-list-widget'),
                        'link' => '',
                    ],
                ],
                'title_field' => '{{{ text }}}',
            ]
        );

        $this->end_controls_section();
    }


    protected function render()
    {
        $settings = $this->get_settings_for_display();

        $this->add_inline_editing_attributes('label_heading', 'basic');
        $this->add_render_attribute(
            'label_heading',
            [
                'class' => ['advertisement__label-heading'],
            ]
        );
        $settings = $this->get_settings_for_display();

        ?>
        <script>
            jQuery(document).ready(function ($) {
                // home slider
            jQuery('#home_slider_video').layerSlider({
                sliderVersion: '6.6.4',
                skin: 'v6',
                type: 'fullsize',
                fullSizeMode: 'normal',
                allowFullscreen: false,
                responsiveUnder: 0,
                // autoPlayVideos: true,
                tnContainerWidth: '100%',
                parallaxScrollReverse: true,
                hideUnder: 0,
                hideOver: 0,
                showCircleTimer: false,
                thumbnailNavigation: 'disabled',
                navStartStop: false,
                fitScreenWidth: true,
                allowRestartOnResize: true,
                loop: false,
                pauseOnHover: false,
                pauseLayers: false,
                autoStart: false,
                navPrevNext: false,
                navButtons: true,
                twoWaySlideshow : true,
                skinsPath: 'wp-content/themes/bgl/assets/css/',

            });

            });


        </script>
        <section id="home_slider_video" class="home-slider images home_slider_arrow" style="height: 100vh">


            <?php
            if (!empty($settings['list_items'])) {
                foreach ($settings['list_items'] as $singleItem) {
                    ?>

                    <!-- Slide 1-->
                    <div class="ls-slide"
                         data-ls="size:cover;duration: 7000; bgposition:50% 50%;  transition2d:12;timeshift:-100;  deeplink:home; kenburnszoom:out; kenburnsrotate:0; kenburnsscale:1.1; parallaxevent:scroll;  parallaxScrollReverse:true; parallaxdurationmove:2500; deeplink:home;   transition2d: 12	;">

                        <!-- Slide background image of the first slide -->
                        <img src="<?= $singleItem['image']['url'] ?>" class="ls-bg" alt="Slide background">

                        <!-- Text layer on second slide -->

                        <div class="ls-layer slider_data"
                             data-ls="offsetyin:50; offsetyout:-50 durationin:5000; delayin:1000; parallaxSensitivity: 10; easingin:easeOutExpo; durationout:2000; parallaxlevel:0;"
                             style="top:90px;right: 0;z-index: 999; left: 0; display: flex !important; height: auto !important">

                            <div class="container">
                                <div class="col-md-12  p-0">
                                    <h3 style="" class="layer_content"><?= $singleItem['text'] ?>
                                    </h3>
                                    <p style="" class=" layer_content"><?= $singleItem['subtitle'] ?> </p>

                                </div>
                            </div>
                        </div>


                    </div>
                    <?php
                }
            }
            ?>


        </section>

        <?php
    }


    protected function content_wrapper(){
        
    }
}
