<?php

namespace WPC\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly


class InnerBanner extends Widget_Base
{

    public function get_name()
    {
        return 'innerbanner';
    }

    public function get_title()
    {
        return 'Inner Banner (arif)';
    }

    public function get_icon()
    {
        return 'eicon-text';
    }

    public function get_categories()
    {
        return ['bgl_category'];
    }

    protected function register_controls()
    {

        $this->start_controls_section(
            'section_content',
            [
                'label' => 'Settings',
            ]
        );

        $this->add_control(
            'heading',
            [
                'label' => 'Label Heading',
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => 'Completed Project'
            ]
        );
        $this->add_control(
            'shortdes',
            [
                'label' => 'Label Heading',
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => 'Plot - 26. Road -04, Block - C, Banani,Dhaka'
            ]
        );

        $this->add_control(
            'title',
            [
                'label' => 'Title',
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => 'NHL Fortuna'
            ]
        );

        $this->add_control(
            'image',
            [
                'label' => esc_html__('Choose Image', 'textdomain'),
                'type' => \Elementor\Controls_Manager::MEDIA,
                'default' => [
                    'url' => \Elementor\Utils::get_placeholder_image_src(),
                ],
            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

        $this->add_inline_editing_attributes('label_heading', 'basic');
        $this->add_render_attribute(
            'label_heading',
            [
                'class' => ['advertisement__label-heading'],
            ]
        );
        $settings = $this->get_settings_for_display();

        // var_dump($settings['image']['url']);
        // exit();

?>


<!--    inner banner -->
<section class="inner-banner" id="home">
<img src="<?=$settings['image']['url']?>" alt="">

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="inner-banner__content heading-global">
                        <h3> <?= $settings['heading'] ?></h3>  
                        <h2><?= $settings['title'] ?></h2>
                        <p><?= $settings['shortdes'] ?></p>
                        
                    </div>
                </div>
            </div>
        </div>
</section>
<!--    inner banner end -->

<?php
    }
}