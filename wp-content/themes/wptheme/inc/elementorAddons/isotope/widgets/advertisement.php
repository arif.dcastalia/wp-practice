<?php

namespace WPC\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly


class Isotope extends Widget_Base
{


    public function get_name()
    {
        return 'isotope';
    }

    public function get_title()
    {
        return 'Isotope(arif)';
    }

    public function get_icon()
    {
        return 'eicon-editor-list-ol';
    }

    public function get_categories()
    {
        return ['bgl_category'];
    }

    protected function register_controls()
    {

        $this->start_controls_section(
            'section_content',
            [
                'label' => 'Settings',
            ]
        );

        $this->add_control(
            'heading',
            [
                'label' => 'Label Heading',
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => 'Isotope'
            ]
        );


        $this->add_control(
            'post to show',
            [
                'label' => 'Total Item to Show',
                'type' => \Elementor\Controls_Manager::NUMBER,
                'default' => '10'
            ]
        );


        $this->end_controls_section();
    }


    protected function render()
    {
        $settings = $this->get_settings_for_display();

        $this->add_inline_editing_attributes('label_heading', 'basic');
        $this->add_render_attribute(
            'label_heading',
            [
                'class' => ['advertisement__label-heading'],
            ]
        );
        $settings = $this->get_settings_for_display();

        $args = array(
            'posts_per_page' => $settings['slide_to_show'] ? $settings['slide_to_show'] : 10,
            'order' => 'ASC',
            'orderby' => 'menu_order',
            'post_type' => 'isotope',
            'order' => 'ASC'
        );
        $myposts = get_posts($args);


?>


<section class="isotope">

    <div class="container" style="padding-top: 120px">
        <div class="row grid">
            <?php
        if ($myposts) {
            foreach ($myposts as $index => $single) {
                $get_img = get_post_meta($single->ID, 'isotope_image', true);
                ?>
            <div class="col-md-4 d-flex grid-item">
                <div class="isotope__content-wrapper">
                    <div class="isotope__content-wrapper__img">
                        <img src="<?= $get_img ? $get_img : get_the_post_thumbnail_url($single->ID) ?>" alt="">
                    </div>
                    <div class="isotope__content-wrapper__text">
                        <p>
                            <?= get_the_title($single->ID) ?>
                        </p>
                    </div>
                </div>
            </div>

            <?php
            }
        }
                ?>


        </div>
    </div>
</section>


<?php if (\Elementor\Plugin::$instance->editor->is_edit_mode()) {
?>
<?php

        } ?>
<?php
    }

}