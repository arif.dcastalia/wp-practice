<?php

namespace WPC\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if (!defined('ABSPATH')) exit; // Exit if accessed directly


class GalleryBox extends Widget_Base
{

    public function get_name()
    {
        return 'gallery';
    }

    public function get_title()
    {
        return 'Gallery (arif)';
    }

    public function get_icon()
    {
        return 'eicon-post-list';
    }

    public function get_categories()
    {
        return ['bgl_category'];
    }

    protected function register_controls()
    {

        $this->start_controls_section(
            'section_content',
            [
                'label' => 'Settings',
            ]
        );

        $this->add_control(
            'label_heading',
            [
                'label' => 'Label Heading',
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => 'Light Gallery'
            ]
        );

        $this->add_control(
            'content_heading',
            [
                'label' => 'Content Heading',
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => 'My Other Example Heading'
            ]
        );


        $this->add_control(
            'gallery',
            [
                'label' => __('Add Images', 'plugin-domain'),
                'type' => \Elementor\Controls_Manager::GALLERY,
                'default' => [],
            ]
        );
        $this->end_controls_section();
    }


    protected function render()
    {
        $settings = $this->get_settings_for_display();

        $this->add_inline_editing_attributes('label_heading', 'basic');
        $this->add_render_attribute(
            'label_heading',
            [
                'class' => ['advertisement__label-heading'],
            ]
        );
        $settings = $this->get_settings_for_display();

        ?>
        <!-- gallery start  -->
        <section class="gallery_section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="subtitle fade-up mb-40"> <?php echo $settings['label_heading'] ?></h3>
                    </div>
                    <div class="clear"></div>
                    <div class="gallery_wrapper gallery_init">

                        <?php
                        foreach ($settings['gallery'] as $image) {

                            ?>

                            <div class="col-md-4">
                                <a href="<?= $image['url'] ?>">
                                    <div class="image_wrapper loader">
                                        <img class="modify-img" data-image-small="<?= $image['url'] ?>"
                                             data-image-large="<?= $image['url'] ?>"
                                             data-image-standard="<?= $image['url'] ?>"
                                             data-src=""
                                             src="<?= get_template_directory_uri() ?>/assets/images/static/blur.jpg"
                                             alt=""/>
                                    </div>
                                </a>
                            </div>
                            <?php
                        }
                        ?>


                    </div>
                </div>
            </div>
        </section>
        <!-- gallery end  -->


        <?php
    }

}
