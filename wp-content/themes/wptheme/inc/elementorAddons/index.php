<?php

function add_elementor_widget_categories( $elements_manager ) {

    $elements_manager->add_category(
        'bgl_category',
        [
            'title' => esc_html__( 'Babylon Group Component', 'bgl' ),
            'icon' => 'fa fa-plug',
        ]
    );
    $elements_manager->add_category(
        'second-category',
        [
            'title' => esc_html__( 'Second Category', 'bgl' ),
            'icon' => 'fa fa-plug',
        ]
    );

}
add_action( 'elementor/elements/categories_registered', 'add_elementor_widget_categories' );
require_once 'ImageLightgallery/custom-elementor.php';
require_once 'inner_banner/custom-elementor.php';
require_once 'next_text/custom-elementor.php';
require_once 'custom_post/custom-elementor.php';
require_once('left_text_bg/custom-elementor.php');
require_once('layer_slider_image/custom-elementor.php');
require_once('about-us/custom-elementor.php');
require_once('isotope/custom-elementor.php');
require_once('gallery/custom-elementor.php');



