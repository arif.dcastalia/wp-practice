<?php

namespace WPC\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly


class LeftTextBg extends Widget_Base
{

    public function get_name()
    {
        return 'lefttextwithbg';
    }

    public function get_title()
    {
        return 'text with bg left img(arif)';
    }

    public function get_icon()
    {
        return 'eicon-text';
    }

    public function get_categories()
    {
        return ['bgl_category'];
    }

    protected function register_controls()
    {

        $this->start_controls_section(
            'section_content',
            [
                'label' => 'Settings',
            ]
        );

        $this->add_control(
            'heading',
            [
                'label' => 'Label Heading',
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => 'Specification
                '
            ]
        );
        $this->add_control(
            'description',
            [
                'label' => 'Label Description',
                'type' => \Elementor\Controls_Manager::TEXTAREA,
                'default' => 'Plot - 26. Road -04, Block - C, Banani,Dhaka'
            ]
        );

        $this->add_control(
            'image',
            [
                'label' => esc_html__('Choose Image', 'textdomain'),
                'type' => \Elementor\Controls_Manager::MEDIA,
                'default' => [
                    'url' => \Elementor\Utils::get_placeholder_image_src(),
                ],
            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

        $this->add_inline_editing_attributes('label_heading', 'basic');
        $this->add_render_attribute(
            'label_heading',
            [
                'class' => ['advertisement__label-heading'],
            ]
        );
        $settings = $this->get_settings_for_display();

        // var_dump($settings['image']['url']);
        // exit();

?>
<!-- detail section start -->

<section class="detail" id="projects">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-7">
                <div class="detail__left">
                    <img src="<?= $settings['image']['url'] ?>" alt="">
                </div>
            </div>

            <div class="col-md-5" style="height: 100%;">
                <div class="detail__right">
                    <h2>
                        <?= $settings['heading'] ?>
                    </h2>
                    <div class="detail__right__table">
                        <table>
                            <tbody>
                            <?= $settings['description'] ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
    </div>
</section>
<!-- detail section end -->

<?php
    }
}
