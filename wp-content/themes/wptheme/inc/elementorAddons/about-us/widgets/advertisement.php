<?php

namespace WPC\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly


class AboutUs extends Widget_Base
{

    public function get_name()
    {
        return 'aboutus';
    }

    public function get_title()
    {
        return 'Aboout us(arif)';
    }

    public function get_icon()
    {
        return 'eicon-text';
    }

    public function get_categories()
    {
        return ['bgl_category'];
    }

    protected function register_controls()
    {

        $this->start_controls_section(
            'section_content',
            [
                'label' => 'Settings',
            ]
        );

        $this->add_control(
            'heading',
            [
                'label' => 'Label Heading',
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => 'About us'
            ]
        );
        $this->add_control(
            'shortdes',
            [
                'label' => 'Label Heading',
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => 'Biswas Builders Limited prides itself as one of the pioneer builders of Bangladesh. At its core, it
                believes in delivering excellence while maintaining a reformed sense of functionality in each
                design.
                As one of the oldest organizations in the industry, Biswas Builders Limited is not only a veteran
                solution to real estate problems but also adds a unique philosophy in the mix to create an
                experience
                that is unforgettable.'
            ]
        );

        $this->add_control(
            'title',
            [
                'label' => 'Title',
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => 'An Organization'
            ]
        );

        $this->add_control(
            'image',
            [
                'label' => esc_html__('Choose Image', 'textdomain'),
                'type' => \Elementor\Controls_Manager::MEDIA,
                'default' => [
                    'url' => \Elementor\Utils::get_placeholder_image_src(),
                ],
            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

        $this->add_inline_editing_attributes('label_heading', 'basic');
        $this->add_render_attribute(
            'label_heading',
            [
                'class' => ['advertisement__label-heading'],
            ]
        );
        $settings = $this->get_settings_for_display();

        // var_dump($settings['image']['url']);
        // exit();

?>

<!-- About US Start -->
<section class="about-us pt-150 pb-150" id="about-us">
      <div class="container">
         <div class="row">

            <div class="col-md-5">
               <div class="about-us__left heading-global">
                  <h3><?= $settings['heading'] ?></h3>

                  <h2><?= $settings['title'] ?></h2>
                  <p><p><?= $settings['shortdes'] ?></p></p>
                  <a href="" class="dc-btn">learn More</a>
               </div>
            </div>


            <div class="col-md-6 offset-md-1 ">
               <div class="about-us__right">
               <img src="<?=$settings['image']['url']?>" alt="">
               </div>
            </div>

         </div>
      </div>
   </section>
   <!-- About US End-->

<?php
    }
}