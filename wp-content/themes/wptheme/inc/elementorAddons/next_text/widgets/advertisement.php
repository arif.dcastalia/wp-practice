<?php

namespace WPC\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly


class NextText extends Widget_Base
{

    public function get_name()
    {
        return 'nexttext';
    }

    public function get_title()
    {
        return 'nextText (arif)';
    }

    public function get_icon()
    {
        return 'eicon-text';
    }

    public function get_categories()
    {
        return ['bgl_category'];
    }

    protected function register_controls()
    {

        $this->start_controls_section(
            'section_content',
            [
                'label' => 'Settings',
            ]
        );

        $this->add_control(
            'text1',
            [
                'label' => 'Label Text1',
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => 'NHL Fortuna is a beacon to the World of luxury and comforts. This is the doorway to a life that you have always wished for-a trendy life mixed and matched with wholesome luxury and superb style. What draw both of your attention and admiration are the magnificence and beauty of the project. At the same time it mirrors the contemporary architecture trend that believes in creation of ornament using the structure and theme of the building rather than the exterior gaudy ornamentation. NHL Fortuna is a beacon to the World of luxury and comforts. This is the doorway to a life that you have always wished for-a trendy life mixed and matched with wholesome luxury and superb style. What draw both of your attention and admiration are the magnificence and beauty of the project.'
            ]
        );
        $this->add_control(
            'shortdes1',
            [
                'label' => 'Label description1',
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => 'NHL Fortuna is located at Plot-26, Road-04, Block-C, Banani, Dhaka. It is a 8 storied residential complex consisting of 14 apartments. You can nest your cars easily at basement floor parking space. Along with its stylish layout and marvelous look. You can have a great provision of privacy and enjoy the maximum benefit of lobby areas. At NHL Fortuna, you can make a choice of your suitable home from apartments approximately 1800 sft. Areas.'
            ]
        );

        $this->add_control(
            'shortdes2',
            [
                'label' => 'Label description2',
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => 'NHL Fortuna is located at Plot-26, Road-04, Block-C, Banani, Dhaka. It is a 8 storied residential complex consisting of 14 apartments. You can nest your cars easily at basement floor parking space. Along with its stylish layout and marvelous look. You can have a great provision of privacy and enjoy the maximum benefit of lobby areas. At NHL Fortuna, you can make a choice of your suitable home from apartments approximately 1800 sft. Areas.'
            ]
        );

        $this->add_control(
            'shortdes3',
            [
                'label' => 'Label description3',
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => 'NHL Fortuna is located at Plot-26, Road-04, Block-C, Banani, Dhaka. It is a 8 storied residential complex consisting of 14 apartments. You can nest your cars easily at basement floor parking space. Along with its stylish layout and marvelous look. You can have a great provision of privacy and enjoy the maximum benefit of lobby areas. At NHL Fortuna, you can make a choice of your suitable home from apartments approximately 1800 sft. Areas.'
            ]
        );


        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

        $this->add_inline_editing_attributes('label_heading', 'basic');
        $this->add_render_attribute(
            'label_heading',
            [
                'class' => ['advertisement__label-heading'],
            ]
        );
        $settings = $this->get_settings_for_display();

        // var_dump($settings['image']['url']);
        // exit();

?>
<!-- text-section -->
<section class="next-text" id="aboutus">
      <img src="assets/images/dynamic/Stickymsg.svg" alt="">
      <div class="container">
         <div class="row">
            <div class="col-md-8 flex_control">
               <div class="next-text__text1">
                  <p>
                  <?= $settings['text1'] ?>
                  </p>
               </div>

            </div>

            <div class="text_section d-flex">
               <div class="next-text__text2">
                  <div class="col-md-4">
                     <div class="next-text__text2__single">
                        <p> <?= $settings['shortdes1'] ?></p>
                     </div>
                  </div>

                  <div class="col-md-4">
                     <div class="next-text__text2__single">
                        <p> <?= $settings['shortdes2'] ?></p>
                     </div>
                  </div>

                  <div class="col-md-4">
                     <div class="next-text__text2__single">
                        <p> <?= $settings['shortdes3'] ?></p>
                     </div>
                  </div>

               </div>

            </div>
         </div>
      </div>
   </section>

   <!-- Text section end -->

<?php
    }
}