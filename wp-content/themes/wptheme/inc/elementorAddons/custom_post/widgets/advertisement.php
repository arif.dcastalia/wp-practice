<?php

namespace WPC\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly


class CustomPost extends Widget_Base
{


    public function get_name()
    {
        return 'custom_post';
    }

    public function get_title()
    {
        return 'Custom Post (arif)';
    }

    public function get_icon()
    {
        return 'eicon-editor-list-ol';
    }

    public function get_categories()
    {
        return ['bgl_category'];
    }

    protected function register_controls()
    {

        $this->start_controls_section(
            'section_content',
            [
                'label' => 'Settings',
            ]
        );

        $this->add_control(
            'heading',
            [
                'label' => 'Label Heading',
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => 'About'
            ]
        );


        $this->add_control(
            'post to show',
            [
                'label' => 'Total Item to Show',
                'type' => \Elementor\Controls_Manager::NUMBER,
                'default' => '10'
            ]
        );


        $this->end_controls_section();
    }


    protected function render()
    {
        $settings = $this->get_settings_for_display();

        $this->add_inline_editing_attributes('label_heading', 'basic');
        $this->add_render_attribute(
            'label_heading',
            [
                'class' => ['advertisement__label-heading'],
            ]
        );
        $settings = $this->get_settings_for_display();

        $args = array(
            'posts_per_page' => $settings['slide_to_show'] ? $settings['slide_to_show'] : 10,
            'order' => 'ASC',
            'orderby' => 'menu_order',
            'post_type' => 'custom_post',
            'order' => 'ASC'
        );
        $myposts = get_posts($args);
        // var_dump($myposts);

?>


<section class="stay-upto-date list ">

    <div class="container">
        <div class="row">
            <div class="Flex_box">
                <?php
        if ($myposts) {
            foreach ($myposts as $index => $single) {
                $get_text = get_post_meta($single->ID, 'custom_text', true);
                $get_img = get_post_meta($single->ID, 'custom_file', true);

                // var_dump($get_img);
                // var_dump($get_text);
                        ?>
                <img src="<?= $get_img ? $get_img : get_the_post_thumbnail_url($single->ID) ?>" alt="">
                
                <p>
                    <?= get_the_title($single->ID) ?>
                </p>
                <p>
                    <?= $get_text ? $get_text : get_the_post_thumbnail_url($single->ID) ?>
                </p>
                <?php
            }
        }
                                ?>

            </div>
        </div>
    </div>
</section>


<?php if (\Elementor\Plugin::$instance->editor->is_edit_mode()) {
        ?>
<?php

        } ?>
<?php
    }

}