<?php
/**
 * bgl_group functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package arif
 */


//update theme requirements
//custom here
@ini_set('upload_max_filesize', '128M');
@ini_set('post_max_size', '128M');
@ini_set('memory_limit', '256M');
@ini_set('max_execution_time', '300');
@ini_set('max_input_time', '300');
@ini_set('max_input_vars', 5000);




// Theme Version
require_once get_template_directory() . '/inc/version.php';



// Theme Features
require_once get_template_directory() . '/inc/themefeatures/all-features.php';




// Theme Siderbar , Widgets
require_once get_template_directory() . '/inc/themefeatures/widgets-list.php';




// Theme Scripts and Jquery
require_once get_template_directory() . '/inc/themefeatures/theme-scripts.php';




//show required plugin install message
require_once get_template_directory() . '/inc/requires-plugin/example.php';




//show required plugin install message
require_once get_template_directory() . '/inc/themefeatures/others-features.php';



//codestar framework registrer
require_once get_theme_file_path() . '/inc/framework/codestar-framework-master/codestar-framework.php';




// themeOption from codestare framework
require_once get_template_directory() . '/inc/themefeatures/theme-option.php';




//for custom elementor widget or addons
require_once get_template_directory() . '/inc/elementorAddons/index.php';






//for custom post and custom taxonomy
require_once get_template_directory() . '/inc/themefeatures/theme-custom-post.php';



//cmb2 activated
require_once __DIR__ . './inc/cmb2/init.php';


//cmb2 field
require_once get_template_directory() . '/inc/custom_meta_field.php';



// for not showing admin bar with admin login view
add_filter('show_admin_bar', '__return_false');



// customization Logo (dynamic logo function)
function my_customization_register($wp_customize)
{
    $wp_customize->add_section(
        'my_header_area',
        array(
            'title' => __('Header Area', 'arif'),
            /// textDomainNameIs'arif'
            'description' => 'You can change your header-icon from here'
        )
    );
    $wp_customize->add_setting(
        'change_logo',
        array(
            'default' => 'https://dcastalia.com/themes/cms/assets/images/static/logo.svg',
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'change_logo',
            array(
                'label' => 'Logo Upload',
                'setting' => 'change_logo',
                'section' => 'my_header_area',
            )
        )
    );

}
add_action('customize_register', 'my_customization_register');


//svg 

function add_file_types_to_uploads($file_types)
{
    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg+xml';
    $file_types = array_merge($file_types, $new_filetypes);
    return $file_types;
}
add_filter('upload_mimes', 'add_file_types_to_uploads');


// laod more 

add_action('wp_footer', 'my_action_javascript'); // Write our JS below here

function my_action_javascript()
{ ?>
<script type="text/javascript">
    jQuery(document).ready(function ($) {

        var page_count = '<?php echo ceil(wp_count_posts('post')->publish / 2); ?>';
        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
        var page = 2;
        jQuery('#load_more').click(function () {
            var data = {
                'action': 'my_action',
                'page': page
            };

            // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
            jQuery.post(ajaxurl, data, function (response) {
                jQuery('#container').append(response);
                if (page_count == page) {

                    jQuery('#load_more').hide();

                }

                page = page + 1;
            });
        });

    });
</script>
<?php
}

add_action('wp_ajax_my_action', 'my_action');
add_action('wp_ajax_nopriv_my_action', 'my_action');

function my_action()
{
    $args = array(
        'post_type' => 'post',
        'paged' => $_POST['page']

    );
    $the_query = new WP_Query($args);

    // The Loop
    if ($the_query->have_posts()) {
        while ($the_query->have_posts()) {
            $the_query->the_post();
            echo '<li>' . get_the_title() . '</li>';
        }

    } else {
        // no posts found

    }
    /* Restore original Post Data */
    wp_reset_postdata();

    wp_die(); // this is required to terminate immediately and return a proper response
}

// pagination for post Here


