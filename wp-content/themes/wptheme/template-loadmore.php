<?php
// Template Name: Load More

get_header();


$args = array(
    'post_type' => 'post',
    'paged' => 1

);

// The Query
$the_query = new WP_Query($args);

// The Loop
if ($the_query->have_posts())
{
    echo '<div style="padding-top:200px;"><ul id="container">';
    while ($the_query->have_posts())
    {
        $the_query->the_post();
        echo '<li>' . get_the_title() . '</li>';
    }
    echo '</ul>
	<a href="javascript:void(0)" id="load_more">Load More</a>
	
	
</div>

    ';
}
else
{
    // no posts found
    
}
/* Restore original Post Data */
wp_reset_postdata();
get_footer();
?>
