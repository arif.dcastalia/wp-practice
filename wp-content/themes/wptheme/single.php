<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package arif
 */

get_header();
?>

    <section class="general_block pb-120 pt-120">
        <div class="container">
            <div class="row">
                <?php
                while ( have_posts() ) :
                    the_post();
                    $url = substr(get_permalink(), strlen(home_url('/')));
                    $parts = explode('/', $url);
                    ?>
                    <div class="col-md-12 header_content">
                        <p class="breadcumb mb-40"><span><a href="<?= get_home_url() ?>">Home</a></span>
                            <?= !empty(get_post_type()) ? get_post_type_object(get_post_type())->labels->name : '' ?>
                        </p>
                        <h3 class="title mb-20">
                            <?=get_the_title()?>
                        </h3>
                        <ul class="type_date">
                            <?php
                            $cat = wp_get_post_categories(get_the_ID());
                            foreach($cat as $singele){
                                $single_cat = get_category($singele);
                                ?>
                                <li><?=$single_cat->name?></li>
                                <?php
                            }
                            ?>
                            <li><?php the_date() ?></li>
                        </ul>
                    </div>
                    <div class="col-md-12 content_wrapper">
                        <div class="img_block mb-40 loader ">
                            <img class="modify-img " data-image-small="<?=get_the_post_thumbnail_url(get_the_ID(),'full');?>"
                                 data-image-large="<?=get_the_post_thumbnail_url(get_the_ID(),'full');?>"
                                 data-image-standard="<?=get_the_post_thumbnail_url(get_the_ID(),'full');?>" data-src=""
                                 src="<?= get_template_directory_uri() ?>/assets/images/static/blur.jpg" alt="">
                        </div>
                        <p>
                            <?php
                            the_content(
                                sprintf(
                                    wp_kses(
                                    /* translators: %s: Name of current post. Only visible to screen readers */
                                        __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'bgl_group' ),
                                        array(
                                            'span' => array(
                                                'class' => array(),
                                            ),
                                        )
                                    ),
                                    wp_kses_post( get_the_title() )
                                )
                            );
                            ?>
                        </p>


                    </div>
                    <div class="col-md-12 share_container">
                        <p>
                            Share:
                        </p>
                        <div class="clear"></div>
                        <ul class="social_list">
                            <li>
                                <a class="hover" href="https://facebook.com" target="_blank">
                                    <svg width="8.885" height="16.471"
                                         viewBox="0 0 8.885 16.471">
                                        <path id="Icon_11_"
                                              d="M8.161,11.319H6.12c-.33,0-.433-.124-.433-.433V8.392c0-.33.124-.433.433-.433H8.161V6.145a4.579,4.579,0,0,1,.557-2.329,3.42,3.42,0,0,1,1.835-1.525,4.586,4.586,0,0,1,1.587-.268h2.02c.289,0,.412.124.412.412v2.35c0,.289-.124.412-.412.412-.557,0-1.113,0-1.67.021a.744.744,0,0,0-.845.845c-.021.618,0,1.216,0,1.855h2.391c.33,0,.454.124.454.454v2.494c0,.33-.1.433-.454.433H11.645v6.72c0,.35-.1.474-.474.474H8.594c-.309,0-.433-.124-.433-.433V11.319Z"
                                              transform="translate(-5.687 -2.023)" fill="#6caf3d"/>
                                    </svg>
                                </a>
                            </li>
                            <li>
                                <a class="hover" href="https://linkedin.com" target="_blank">
                                    <svg width="16.471" height="16.471"
                                         viewBox="0 0 16.471 16.471">
                                        <g id="Group_17722" data-name="Group 17722" transform="translate(0 0)">
                                            <path id="Path_1569" data-name="Path 1569"
                                                  d="M18.3,18.322V12.289c0-2.965-.638-5.229-4.1-5.229a3.575,3.575,0,0,0-3.232,1.771h-.041v-1.5H7.66V18.322h3.418V12.866c0-1.441.268-2.821,2.038-2.821,1.75,0,1.771,1.626,1.771,2.9V18.3H18.3Z"
                                                  transform="translate(-1.833 -1.851)" fill="#6caf3d"/>
                                            <path id="Path_1570" data-name="Path 1570" d="M2.26,7.32H5.678V18.314H2.26Z"
                                                  transform="translate(-1.992 -1.844)" fill="#6caf3d"/>
                                            <path id="Path_1571" data-name="Path 1571"
                                                  d="M3.976,2A1.987,1.987,0,1,0,5.953,3.976,1.977,1.977,0,0,0,3.976,2Z"
                                                  transform="translate(-2 -2)" fill="#6caf3d"/>
                                        </g>
                                    </svg>
                                </a>
                            </li>
                            <li>
                                <a class="hover" href="https://twitter.com" target="_blank">
                                    <svg width="17" height="13.834"
                                         viewBox="0 0 17 13.834">
                                        <path id="_x30_4.Twitter"
                                              d="M18.733,4.7a7.149,7.149,0,0,1-2,.553,3.4,3.4,0,0,0,1.53-1.934,7.339,7.339,0,0,1-2.21.85,3.488,3.488,0,0,0-6.035,2.38,3.187,3.187,0,0,0,.085.786A9.81,9.81,0,0,1,2.944,3.7,3.5,3.5,0,0,0,4.006,8.354a3.346,3.346,0,0,1-1.572-.425,3.522,3.522,0,0,0,2.784,3.464,3.825,3.825,0,0,1-1.572.064A3.5,3.5,0,0,0,6.9,13.879a7.021,7.021,0,0,1-5.164,1.445A9.831,9.831,0,0,0,7.087,16.9a9.83,9.83,0,0,0,9.9-9.9,3.138,3.138,0,0,0-.021-.446A6.917,6.917,0,0,0,18.733,4.7Z"
                                              transform="translate(-1.732 -3.063)" fill="#6caf3d"/>
                                    </svg>
                                </a>
                            </li>

                            <li>
                                <a class="hover" href="https://youtube.com" target="_blank">
                                    <svg width="18.824" height="13.334"
                                         viewBox="0 0 18.824 13.334">
                                        <g id="Group_14060" data-name="Group 14060" transform="translate(0 0)">
                                            <path id="Path_6456" data-name="Path 6456"
                                                  d="M18.561,4.224A2.1,2.1,0,0,0,16.873,2.5a60.494,60.494,0,0,0-14.921,0A2.1,2.1,0,0,0,.262,4.224a38.459,38.459,0,0,0,0,8.97,2.1,2.1,0,0,0,1.689,1.72,60.494,60.494,0,0,0,14.921,0,2.1,2.1,0,0,0,1.688-1.72A38.454,38.454,0,0,0,18.561,4.224ZM7.843,11.846V5.572l4.706,3.137Z"
                                                  transform="translate(0 -2.041)" fill="#6caf3d"/>
                                        </g>
                                    </svg>
                                </a>
                            </li>
                        </ul>

                    </div>
                    <?php

//                    the_post_navigation(
//                        array(
//                            'prev_text' => '<span class="nav-subtitle">' . esc_html__( 'Previous:', 'bgl_group' ) . '</span> <span class="nav-title">%title</span>',
//                            'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Next:', 'bgl_group' ) . '</span> <span class="nav-title">%title</span>',
//                        )
//                    );

                    // If comments are open or we have at least one comment, load up the comment template.
//                    if ( comments_open() || get_comments_number() ) :
//                        comments_template();
//                    endif;

                endwhile; // End of the loop.
                ?>
            </div>
        </div>
    </section>

<?php
get_footer();
