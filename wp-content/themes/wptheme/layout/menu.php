<?php
?>

<!--===================== fixed section start =====================-->
<!-- <section class="menu-bar menu-fixed"> -->
   <nav class="navbar navbar-dark navbar-expand-md menu-bar">
      <div class="container">


         <a href="<?= esc_url(home_url()) ?>" class="navbar-brand">
            <img src="<?php echo get_theme_mod('change_logo'); ?>" alt="mainLogo">
         </a>
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive">
            <sapn class="navbar-toggler-icon">
            </sapn>
         </button>

         <div class="collapse navbar-collapse bgcolor" id="navbarResponsive">
            <ul class="navbar-nav ml-auto float-right">
               <li class="nav-items"><a href="#home" class="nav-link">Home</a></li>
               <li class="nav-items"><a href="#aboutus" class="nav-link">About Us</a></li>
               <li class="nav-items"><a href="#projects" class="nav-link">Projects</a></li>
               <li class="nav-items"><a href="#features" class="nav-link">Services</a></li>
               <li class="nav-items"><a href="#career" class="nav-link">Career</a></li>
               <li class="nav-items"><a href="#contact" class="nav-link">Contact</a></li>
            </ul>
         </div>
      </div>
   </nav>
<!--===================== fixed section end =====================-->

