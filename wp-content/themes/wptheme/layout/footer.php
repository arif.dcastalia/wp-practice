<?php
?>


   <section id="career" class="footer">
      <div class="container">
         <div class="row">
            <div class="col-sm-2">
               <ul class="footer__social">
                  <li>
                     <a href="#">
                        <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 576 512"
                           height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
                           <path
                              d="M549.655 124.083c-6.281-23.65-24.787-42.276-48.284-48.597C458.781 64 288 64 288 64S117.22 64 74.629 75.486c-23.497 6.322-42.003 24.947-48.284 48.597-11.412 42.867-11.412 132.305-11.412 132.305s0 89.438 11.412 132.305c6.281 23.65 24.787 41.5 48.284 47.821C117.22 448 288 448 288 448s170.78 0 213.371-11.486c23.497-6.321 42.003-24.171 48.284-47.821 11.412-42.867 11.412-132.305 11.412-132.305s0-89.438-11.412-132.305zm-317.51 213.508V175.185l142.739 81.205-142.739 81.201z">
                           </path>
                        </svg>
                     </a>
                  </li>

                  <li>
                     <a href="#">
                        <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 320 512"
                           height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
                           <path
                              d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z">
                           </path>
                        </svg>
                     </a>
                  </li>

                  <li>
                     <a href="#">
                        <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 448 512"
                           height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
                           <path
                              d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z">
                           </path>
                        </svg>
                     </a>
                  </li>
               </ul>
            </div>

            <div class="pl-0 col-sm-9 offset-sm-1">
               <div class="footer__menu d-flex flex-wrap">

                  <div class="col-sm-4">
                     <h4>Corporate Office</h4>
                     <p>Awal center (8th Floor) <br> 34 Kemal Ataturk Avenue <br> Banani, Dhaka 1213</p>
                  </div>

                  <div class="pl-30 col-sm-4">
                     <h4>About Us</h4>
                     <ul>
                        <li><a href="/about">About Us</a></li>
                        <li><a href="/team">Our Team</a></li>
                     </ul>
                  </div>

                  <div class="col-sm-4 pl-30">
                     <h4>Services</h4>
                     <ul>
                        <li><a href="#">Landscaping</a></li>
                        <li><a href="#">Contracting Maintenance</a></li>
                        <li><a href="#">Consultancy</a></li>
                        <li><a href="#">Civil Construction</a></li>
                     </ul>
                  </div>

                  <div class="contact-numbers col-sm-4">
                     <h4>Contact</h4>
                     <ul>
                        <li><a href="tel:02 222275018">02 222275018</a></li>
                        <li><a href="tel:02 222275610">02 222275610</a></li>
                        <li><a href="mailto:info@nhlbd.org">info@nhlbd.org</a></li>
                     </ul>
                  </div>

                  <div class="pl-30 col-sm-4">
                     <h4>Projects</h4>
                     <ul>
                        <li><a href="/projects/ongoing">Ongoing</a></li>
                        <li><a href="/projects/upcoming">Upcoming</a></li>
                        <li><a href="/projects/completed">Completed</a></li>
                     </ul>
                  </div>

               </div>
            </div>
         </div>

         <div class="footer__copyright row">
            <div class="col-sm-3">
               <h4> © 2022 Nassa Holdings Limited.</h4>
            </div>
            <div class="d-flex justify-content-between col-sm-9">
               <p>All Rights Reserved.</p>
               <a href="https://dcastalia.com/" target="_blank">Designed &amp; Developed by
                  <span>Dcastalia</span>
               </a>
            </div>
         </div>
      </div>
   </section>


   <!-- Footer End  -->






   <!---------- js links ---------->
   <script src="assets/js/jquery.min.js"></script>
   <script src="assets/lib/bootstrap/bootstrap.min.js"></script>
   <script src="assets/lib/light-gallery/lightgallery.js"></script>
   <script src="assets/lib/layer-slider/layerslider.transitions.js"></script>
   <script src="assets/lib/layer-slider/layerslider.kreaturamedia.jquery.js"></script>
   <script src="assets/lib/slick-slider/slick.js"></script>

   <script src="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js"></script>

   <script src="assets/lib/gsap/tweenmax.js"></script>
   <script src="assets/lib/anim/jquery.blast.min.js"></script>
   <script src="assets/lib/nice-select/jquery.nice-select.min.js"></script>
   <script src="assets/lib/dc/image-preloader.js"></script>
   <script src="assets/lib/dc/dc-animation.js"></script>
   <script src="assets/js/global.js"></script>
   <script src="assets/js/home.js"></script>
</body>

</html>