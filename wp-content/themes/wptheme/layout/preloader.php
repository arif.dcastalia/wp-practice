
<div id="load" class="style_two preloader">
    <svg id="Component_88_99" data-name="Component 88 – 99" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="100" height="60" viewBox="0 0 100 60">
        <defs>
            <clipPath id="clip-path">
                <rect id="logo" width="100" height="60" transform="translate(98 20)" fill="#fff"></rect>
            </clipPath>
        </defs>
        <g id="Mask_Group_188" data-name="Mask Group 188" transform="translate(-98 -20)" clip-path="url(#clip-path)">
            <g id="Babylon_logo" data-name="Babylon logo" transform="translate(99.238 21.792)">
                <path id="Path_6365" data-name="Path 6365" d="M.2,54.79S2.219,35.347,17.773,32.356c2.642-.523,11.766-2.268,21.138,7.553.349-2.443-.474-27.769,23.98-34.5,7.453-1.77,18.5-1.57,31.06,11.118C90.985,12.115,80.266-5.933,56.859,1.969,49.182,4.337,36.593,16.053,37.142,33.876,33.1,30.711,26.3,24.753,14.931,27.495,11.217,28.392-1.72,35.173.2,54.79" fill="#50b248"></path>
                <g id="Group_13936" data-name="Group 13936">
                    <g id="Group_13935" data-name="Group 13935">
                        <text id="BABYLON" transform="translate(10.517 53.423)" fill="#575e64" font-size="16" font-family="Helvetica-Bold, Helvetica" font-weight="700">
                            <tspan x="0" y="0"><?=esc_html('BABYLON') ?></tspan>
                        </text>
                    </g>
                </g>
            </g>
        </g>
    </svg>

    <img src="<?=get_template_directory_uri()?>/assets/images/static/pre_loader.gif" alt="" id="load" >
</div>