<meta charset="UTF-8"/>
<meta
    name="viewport"
    content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"
/>
<meta http-equiv="X-UA-Compatible" content="ie=edge"/>
<link rel="shortcut icon" href="<?=get_template_directory_uri()?>/assets/images/static/fav.png"/>
<meta name="theme-color" content="#009975"/>
<title><?=$pageTitle?></title>
<link rel="stylesheet" href="https://use.typekit.net/ihd6mmf.css"/>
<!-------- css links start -------->
<link rel="stylesheet" href="assets/css/bootstrap.min.css"/>
<link rel="stylesheet" href="assets/css/slick.min.css"/>
<link rel="stylesheet" href="assets/css/dc-animation.css"/>
<link rel="stylesheet" href="assets/css/layerslider.css"/>
<link rel="stylesheet" href="assets/css/home.css"/>
<!---------- css links end -------->
