<?php
// Template Name: form

get_header();
?>
<form method="POST" style="padding-top: 120px;">

  <div class="form-group">
    <label>Enter Name</label>
    <input type="text" class="form-control" placeholder="Name" name="fullname">
  </div>
  <div class="form-group">
    <label>Class</label>
    <input type="text" class="form-control" placeholder="Class" name="schoolclass">
  </div>

  <div class="form-group">
    <label>Marks</label>
    <input type="text" class="form-control" placeholder="Marks" name="marks">
  </div>
  <input type="submit" class="btn btn-primary" name="insert" value="Submit">
</form>

<?php 
if(isset($_POST['insert'])){
    $name = $_POST['fullname'];
    $class = $_POST['schoolclass'];
    $marks = $_POST['marks'];

    global $wpdb;
    $query = $wpdb->insert("form_test",array(
        "name" => $name,
        "class" => $class,
        "marks" => $marks
    ) );

    if($query == true){
        echo "<script> alert('Data Inserted successfully')</script>";
    }
    else{
        echo "<script> alert('Data Not Inserted')</script>";
    }

}

?>

<?php
get_footer();

?>