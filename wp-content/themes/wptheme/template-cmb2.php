<?php
//banner section
$get_img = get_post_meta(get_the_ID(), 'banner_image', true);
$get_title = get_post_meta(get_the_ID(), 'title', true);
$get_sub = get_post_meta(get_the_ID(), 'sub_title', true);
$get_shortdes = get_post_meta(get_the_ID(), 'short_des', true);

//about section
$get_about_img = get_post_meta(get_the_ID(), 'about_us_image', true);
$get_heading_text = get_post_meta(get_the_ID(), 'heading_text', true);
$get_left_text = get_post_meta(get_the_ID(), 'left_text', true);
$get_middle_text = get_post_meta(get_the_ID(), 'middle_text', true);
$get_right_text = get_post_meta(get_the_ID(), 'right_text', true);

//details page
$get_details_img = get_post_meta(get_the_ID(), 'details_Background_img', true);
$get_details_title = get_post_meta(get_the_ID(), 'details_title', true);
$get_details_table = get_post_meta(get_the_ID(), 'details_table', true);
?>

<?php
// Template Name: CMB2
get_header();
?>

<!-- banner section -->
<section class="inner-banner" id="home">
    <img src="<?= $get_img ?>" alt="">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="inner-banner__content heading-global">
                    <h3>
                        <?= $get_title ?>
                    </h3>
                    <h2>
                        <?= $get_sub ?>
                    </h2>
                    <p>
                        <?= $get_shortdes ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- about us section -->
<section class="next-text" id="aboutus">
    <img src="<?= $get_about_img ?>" alt="" alt="">
    <div class="container">
        <div class="row">
            <div class="col-md-8 flex_control">
                <div class="next-text__text1">
                    <p>
                        <?= $get_heading_text ?>
                    </p>
                </div>
            </div>
            <div class="text_section d-flex">
                <div class="next-text__text2">
                    <div class="col-md-4">
                        <div class="next-text__text2__single">
                            <p>
                                <?= $get_left_text ?>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="next-text__text2__single">
                            <p>
                                <?= $get_middle_text ?>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="next-text__text2__single">
                            <p>
                                <?= $get_right_text ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- details section Start -->
<section class="detail" id="projects">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="detail__left">
                    <img src="<?= $get_details_img ?>" alt="">
                </div>
            </div>
            <div class="col-md-6">
                <div class="detail__right">
                    <h2>
                        <?= $get_details_title ?>
                    </h2>
                    <div class="detail__right__table">
                        <table>
                            <tbody>
                                <?= $get_details_table ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- detail section end -->

<?php
get_footer();
?>

<!-- There are two area that I need to inprove
    ======> Slider
    ======> Load More 
-->