(function ($,elementor) {
    var windowWidth = $(window).width();
    var TM = TweenMax;
    var tl = new TimelineMax();
    var container_left = $('.menu-bar').offset().left;



    if ($('.strategy_init').length > 0) {
        $('.strategy_init').css('margin-left', container_left);
    }

    if ($('.left_align_upcoming_project').length > 0) {
        $('.left_align_upcoming_project').css('padding-left', container_left + 15);
    }

    if ($('.timeline_gap').length > 0) {
        $('.timeline_gap').css('padding-right',container_left + 15);
    }




    if($('.loading-bar').length > 0) {
        $(".loading-bar").slick({
            centerPadding: "80px",
            dots: false,
            infinite: true,
            speed: 1000,
            slidesToShow: 5,
            slidesToScroll: 1,
            focusOnSelect: true,
            prevArrow: $('#timeline_prev'),
            nextArrow: $('#timeline_next'),
            arrows: true,
            draggable: true,
            cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 0.7)',
            asNavFor: ".labels",
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },

            ]

        });



        $(".labels").slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            draggable: false,
            speed: 1000,
            asNavFor: ".loading-bar",

        });


    }




    // sustainablity slider
    if ($('.slider_init').length > 0) {
        // $('.slider_init').not('.slick-initialized').slick({
        //     infinite: true,
        //     slidesToShow: 2,
        //     slidesToScroll: 1,
        //     autoplay: false,
        //     autoplaySpeed: 2000,
        //     speed: 1000,
        //     prevArrow: $('#sustain_prev'),
        //     nextArrow: $('#sustain_next'),
        //     responsive: [
        //         {
        //             breakpoint: 991,
        //             settings: {
        //                 slidesToShow: 2,
        //                 slidesToScroll: 1
        //             }
        //         },
        //         {
        //             breakpoint: 768,
        //             settings: {
        //                 slidesToShow: 1,
        //                 slidesToScroll: 1
        //             }
        //         }
        //     ]
        // });
    }


    if ($('.strategy_init').length > 0) {
        // $('.strategy_init').not('.slick-initialized').slick({
        //     infinite: true,
        //     slidesToShow: 3,
        //     slidesToScroll: 1,
        //     autoplay: false,
        //     speed: 1000,
        //     autoplaySpeed: 2000,
        //     prevArrow: $('.stay-upto-date-prev'),
        //     nextArrow: $('.stay-upto-date-next'),
        //     responsive: [
        //         {
        //             breakpoint: 991,
        //             settings: {
        //                 slidesToShow: 2,
        //                 slidesToScroll: 1
        //             }
        //         },
        //         {
        //             breakpoint: 768,
        //             settings: {
        //                 slidesToShow: 1,
        //                 slidesToScroll: 1
        //             }
        //         }
        //     ]
        // });
    }


    if ($('.our-membership__section-container__section-row__init').length > 0) {
        // $('.our-membership__section-container__section-row__init').not('.slick-initialized').slick({
        //     infinite: true,
        //     slidesToShow: 5,
        //     slidesToScroll: 1,
        //     autoplay: false,
        //     autoplaySpeed: 2000,
        //     speed: 1000,
        //     prevArrow: $('.our-membership-prev'),
        //     nextArrow: $('.our-membership-next'),
        //     responsive: [
        //         {
        //             breakpoint: 991,
        //             settings: {
        //                 slidesToShow: 3,
        //                 slidesToScroll: 1
        //             }
        //         },
        //         {
        //             breakpoint: 768,
        //             settings: {
        //                 slidesToShow: 3,
        //                 slidesToScroll: 1
        //             }
        //         }
        //     ]
        // });
    }



    // if ($('.upcoming_slider_init').length > 0) {
    //     $('.upcoming_slider_init').not('.slick-initialized').slick({
    //         infinite: true,
    //         slidesToShow: 1,
    //         slidesToScroll: 1,
    //         autoplay: false,
    //         speed: 1000,
    //         autoplaySpeed: 2000,
    //         asNavFor: '.nav_slider',
    //         prevArrow: $('#upcoming_prev'),
    //         nextArrow: $('#upcoming_next'),
    //     });
    // }
    // $('.nav_slider').not('.slick-initialized').slick({
    //     slidesToShow: 1,
    //     slidesToScroll: 1,
    //     speed: 1000,
    //     asNavFor: '.upcoming_slider_init',
    //     dots: false,
    //     focusOnSelect: true
    // });


    if(windowWidth <= 767){
        if($('#activities_data_load').length > 0){
            $('#activities_data_load').not('.slick-initialized').slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                drag:true,
                speed: 1000,
                autoplaySpeed: 2000,
            });
        }
    }

    $(window).on('resize', function () {
        var container_left = $('.menuBar .menuBar__logo').offset().left;

        if ($('.strategy_init').length > 0) {
            $('.strategy_init').css('margin-left', container_left);

        }
        if ($('.left_align_upcoming_project').length > 0) {
            $('.left_align_upcoming_project').css('padding-left', container_left + 15);

        }
    });
    $(window).on('load', function () {
        var container_left = $('.menuBar .menuBar__logo').offset().left;

        if ($('.strategy_init').length > 0) {
            $('.strategy_init').css('margin-left', container_left);

        }
        if ($('.left_align_upcoming_project').length > 0) {
            $('.left_align_upcoming_project').css('padding-left', container_left + 15);

        }


        //set active class to first slide
        if(windowWidth > 767 ){
            if($('.loading-bar').length > 0){
                console.log($('.loading-bar .slick-active').length)
                $('.loading-bar  .slick-active').eq(4).addClass('slick-current');;
                $('.loading-bar .slick-active').eq(0).removeClass('slick-current');
            }

        }
    });



    // faq expand
    $('.visa-faq__content__single').each(function () {
        $(this).find('.visa-faq__content__single__title').click(function () {
            $(".visa-faq__content__single__title").not(this).next('div').removeClass('open');
            $(".visa-faq__content__single__title").not(this).next('div').hide('slow');

            $(".visa-faq__content__single__title").not(this).parent().removeClass('expand')
            $(this).next('div').slideToggle('slow', function () {
                // $(this).parent().removeClass('open')
                $('.visa-faq__content__single').removeClass('open')

            })
            if ($(this).parent().hasClass('open')) {
                $(this).parent().toggleClass('')

            } else {
                $(this).parent().toggleClass('expand')

            }
        })
    })


    // Board Of Director modal start
    if ($(".management-modal__dialog__content").length > 0) {
        $(".management-modal__dialog__content").css("padding-left", container_left);
        $(".management-modal__dialog__content").css("padding-right", container_left);

        $(window).on("resize", function () {
            $(".management-modal__dialog__content").css("padding-left", container_left);
            $(".management-modal__dialog__content").css("padding-right", container_left);
        });
    }

    if ($(".management-modal__dialog__content__body__image-wapper").length > 0) {
        // $(".management-modal__dialog__content__body__image-wapper").css("padding-left", container_left);
        $(".management-modal__dialog__content__body__image-wapper").css("padding-right", container_left + 15);

        $(window).on("resize", function () {
            // $(".management-modal__dialog__content__body__image-wapper").css("padding-left", container_left);
            $(".management-modal__dialog__content__body__image-wapper").css("padding-right", container_left + 15);
        });
    }
    // Board Of Director modal end


    $("#career-form").on("change", ".file-upload-field", function () {
        $(this).parent(".upload_cv_inner").attr("data-text", $(this).val().replace(/.*(\/|\\)/, ""));
    });

})(jQuery, window.elementorFrontend);

//isotope
$('.grid').isotope({
    // options
    itemSelector: '.grid-item',
    layoutMode: 'fitRows'
  
  });

// =================================================================

let getAllselector = document.querySelectorAll("section");
let getLi = document.querySelectorAll("nav ul li");
// getLi[0].classList.add("active");
// console.log(getLi);



window.addEventListener("scroll", () => {
  getAllselector.forEach((e, i) => {
    if (pageYOffset >= e.offsetTop - 180) {

      // console.log(e.offsetTop);

      if (document.querySelector("section.active")) {
        document.querySelector("section.active").classList.remove("active");
      }
      if (document.querySelector("li.active")) {
        document.querySelector("li.active").classList.remove("active");
      }

      getAllselector[i].classList.add("active");
      getLi[i].classList.add("active");
    }

  })

})