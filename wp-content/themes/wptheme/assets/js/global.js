/* =================================================================
* Template Master JS
*
* Template:		Babylon Group HTML Website Template
* Author:		Dcastalia
* URL:			https://dcastalia.com/
*
================================================================= */

/* Table of Content
====================
* Rev Slider
* Testimonial Slider Init
* Client Slider Init
* Portfolio filter
*/


(function (jQuery) {
    "use strict";
    var windowWidth = jQuery(window).width();
    var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Nokia|Opera Mini/i.test(navigator.userAgent) ? true : false;
    var container_left = jQuery('.menuBar .menuBar__logo').offset().left;

    // Add class "is-mobile" to </body>
    if (isMobile) {
        jQuery("body").addClass("is-mobile");
    }
    var window_width = jQuery(window).width();

    var TM = TweenMax;
    var tl = gsap.timeline();;
    console.log("Designed & Developed by Dcastalia");
    jQuery("body").prepend('<div class="overlay"></div><div class="form-overlay"></div>');

    if(jQuery('.parallax-bg').length > 0) {
        jQuery('.parallax-bg').dParallax();
    }

    if (jQuery(".pre_loader").length > 0) {
        // var jQueryloader = document.querySelector(".pre_loader");
        //
        // window.onload = function () {
        //     window.setTimeout(function () {
        //         jQueryloader.classList.remove("pre_loader--active");
        //         let revealContainers = document.querySelectorAll(".reveal_logo");
        //
        //
        //
        //     }, 1500);
        // };
        // gsap.to("#preload_logo", 0, {   ease: "power2",
        //     duration: 1,opacity:1});
        // if (jQuery(".anim-link").length > 0) {
        //     document.querySelector(".anim-link").addEventListener("click", function () {
        //         jQueryloader.classList.add("pre_loader--active");
        //         gsap.to("#preload_logo", 0.7, {   ease: "power2",
        //             duration: 1,opacity:1});
        //         window.setTimeout(function () {
        //             jQueryloader.classList.remove("pre_loader--active");
        //
        //         }, 1500);
        //     });
        // }
    }


    // counter

    function DataCounter() {
        jQuery('.countIt').each(function (e) {
            var start = jQuery(this).attr("data-count-start");
            var end = jQuery(this).attr("data-count-end");
            jQuery(this).countTo({
                from: start,
                to: end,
                speed: 1200,
                onComplete: null
            });
        })

    }

    var CheckOnce = false;

    if (jQuery(".counter").length > 0) {
        jQuery(window).scroll(function () {
            if (jQuery(window).scrollTop() >= jQuery(".counter").offset().top - 350) {
                if (CheckOnce == false) {
                    DataCounter();
                    CheckOnce = true;
                }
            }
        });
    }


    //------------ Light gallery
    if (jQuery(".Light").length > 0) {
        jQuery(".Light").lightGallery({
            selector: "a",
        });
    }

    //------------ Light gallery with thumbnail
    if (jQuery(".LightThumb").length > 0) {
        jQuery(".LightThumb").lightGallery({
            selector: "a",
            exThumbImage: "data-exthumbimage",
        });
    }

    //------------ nice select
    if (jQuery(".Select").length > 0) {
        jQuery(".Select select").niceSelect();
    }

    //------------ tab change in mobile using nice select
    jQuery(".TabSelect").on("change", function (e) {
        jQuery(".TabMenus li a").eq(jQuery(this).val()).tab("show");
    });

    //------ form validation
    jQuery("form .dynamic_submit_btn").click(function () {
        jQuery(".form-overlay").addClass("doit");
    });

    jQuery(document).on("click", ".form-overlay.doit,.ok-class", function () {
        jQuery(".form-overlay.doit, .form-message-container").hide();
    });

    jQuery(".btn , button").click(function () {
        jQuery(".form-overlay.doit, .form-message-container").removeAttr("style");
    });

    jQuery(".dynamic_submit_btn").on("click", function () {
        setTimeout(function () {
            jQuery(".form-overlay.doit").hide();
        }, 15000);
    });
    //------ form validation


    // scroll to section
    jQuery(".scroll-to").click(function () {
        if (
            location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") &&
            location.hostname == this.hostname
        ) {
            var jQuerytarget = jQuery(this.hash);
            jQuerytarget = (jQuerytarget.length && jQuerytarget) || jQuery("[name=" + this.hash.slice(1) + "]");
            if (jQuerytarget.length) {
                var targetOffset = jQuerytarget.offset().top;
                jQuery("html,body").animate({scrollTop: targetOffset}, 1000);
                return false;
            }
        }
    });

    // disable scroll
    jQuery(".Overlay,.menuItems").bind("mousewheel DOMMouseScroll hover", function (e) {
        var scrollTo = null;

        if (e.type == "mousewheel") {
            scrollTo = e.originalEvent.wheelDelta * -1;
        } else if (e.type == "DOMMouseScroll") {
            scrollTo = 40 * e.originalEvent.detail;
        }

        if (scrollTo) {
            e.preventDefault();
            jQuery(this).scrollTop(scrollTo + jQuery(this).scrollTop());
        }
    });

    //------- message box start

    jQuery(document).delegate(".msg_cont", "click", function () {
        open_msg();
    });

    jQuery(document).delegate(".msg_icon", "click", function () {
        open_msg();
    });

    function open_msg() {
        jQuery(".msg_cont_wrap").css({width: "330px", height: "446px"});
        TM.to(".msg_cont", 0.2, {
            height: 580,
            width: 580,
            right: -86,
            bottom: -86,
            borderRadius: 290,
            background: "rgba(255, 255, 255, 1)",
            onComplete: function () {
            },
        });

        TM.to(".msg_form", 0.5, {
            right: 0,
            delay: 0.2,
            onComplete: function () {
                jQuery(".msg_cont_wrap").addClass("bx_shadow");
            },
        });

        jQuery(".msg_cont_wrap").addClass("msg_opened");
        jQuery(".msg_cont_wrap").removeClass("msg_closed");
    }

    jQuery(document).delegate(".close_btn", "click", function () {
        close_msg();
    });

    function close_msg() {
        jQuery(".msg_cont_wrap").removeClass("bx_shadow");

        TM.to(".msg_cont", 0.2, {
            width: "50px",
            height: "50px",
            right: 35,
            bottom: 8,
            borderRadius: "100%",
            background: "#008C44",
            onComplete: function () {
                setTimeout(function () {
                    jQuery(".msg_cont_wrap").css({width: "120px", height: "120px"});
                }, 500);
            },
        });

        TM.to(".msg_form", 0.5, {
            right: -500,
        });

        setTimeout(function () {
            jQuery(".msg_cont_wrap").removeClass("msg_opened");
            jQuery(".msg_cont_wrap").addClass("msg_closed");
        }, 500);
    }

    jQuery(".msg_cont , .msg_icon").click(function () {
        jQuery(".msg_cont_wrap .title").fadeIn(2000);
        jQuery(".close_btn").fadeIn();
    });

    jQuery(".close_btn").click(function () {
        jQuery(".msg_cont_wrap .title,.close_btn").hide();
    });

    // image preloader on slider
    jQuery(".img-preload").length > 0 &&
    jQuery(".img-preload").on("afterChange", function (event, slick, currentSlide) {
        imgPreloader();
    });

    //------------ message box end


    // hover link
    var hoverMouse = function (jQueryel) {
        jQueryel.each(function () {
            var jQueryself = jQuery(this);
            var hover = false;
            var offsetHoverMax = jQueryself.attr("offset-hover-max") || 0.7;
            var offsetHoverMin = jQueryself.attr("offset-hover-min") || 0.5;

            var attachEventsListener = function () {
                jQuery(window).on("mousemove", function (e) {
                    //
                    var hoverArea = hover ? offsetHoverMax : offsetHoverMin;

                    // cursor
                    var cursor = {
                        x: e.clientX,
                        y: e.clientY + jQuery(window).scrollTop()
                    };

                    // size
                    var width = jQueryself.outerWidth();
                    var height = jQueryself.outerHeight();

                    // position
                    var offset = jQueryself.offset();
                    var elPos = {
                        x: offset.left + width / 2,
                        y: offset.top + height / 2
                    };

                    // comparaison
                    var x = cursor.x - elPos.x;
                    var y = cursor.y - elPos.y;

                    // dist
                    var dist = Math.sqrt(x * x + y * y);

                    // mutex hover
                    var mutHover = false;

                    // anim
                    if (dist < width * hoverArea) {
                        mutHover = true;
                        if (!hover) {
                            hover = true;
                        }
                        onHover(x, y);
                    }

                    // reset
                    if (!mutHover && hover) {
                        onLeave();
                        hover = false;
                    }
                });
            };

            var onHover = function (x, y) {
                TweenMax.to(jQueryself, 0.4, {
                    x: x * 0.8,
                    y: y * 0.8,
                    //scale: .9,
                    rotation: x * 0.05,
                    ease: Power2.easeOut
                });
            };
            var onLeave = function () {
                TweenMax.to(jQueryself, 0.7, {
                    x: 0,
                    y: 0,
                    scale: 1,
                    rotation: 0,
                    ease: Elastic.easeOut.config(1.2, 0.4)
                });
            };

            attachEventsListener();
        });
    };
    hoverMouse(jQuery('.dc-link,#hamburgetMain'));


    // menu

    jQuery('.menuItems .menu_image ul').css('margin-left', container_left + 15);

    jQuery(window).on('resize', function(){
        var container_left = jQuery('.menuBar .menuBar__logo').offset().left;

        jQuery('.menuItems .menu_image ul').css('margin-left', container_left + 15);
;    });
    jQuery(window).on('load', function(){
        var container_left = jQuery('.menuBar .menuBar__logo').offset().left;

        jQuery('.menuItems .menu_image ul').css('margin-left', container_left + 15);
;    });

    jQuery('#hamburgetMain').click(function () {
        tl.to('.menuItems', {display: 'block'},'-=0.05')
            .to('.menuItems', {width: '100%', rotationZ: 0,
                force3D: false,duration:'0.6',ease: "power2.out",maxWidth: '100%', opacity: 1})
            .to('.menuItems .menu_image ul li', {autoAlpha: 1,ease: "power2.out"},'-=5')
            .to('.menuItems .menuBar__logo', 0.1, {autoAlpha: 1,ease: "power2.out"},'-=5')
            .staggerTo('.menuItems .mymenu .menu-link', 0.5, {autoAlpha:1,stagger:0.05,pause:true,ease: "Power4.out",y:0},'-=9')
            .to('.menuItems .menuBar__hamburger', 0.1, {autoAlpha: 1,ease: "power2.out"},'-=3')

    });

    jQuery('#menuClose').click(function () {
        tl.to(".menuItems .menuBar__logo", {
            autoAlpha: 0,
        },'-=0.1')
            .staggerTo('.menuItems .mymenu .menu-link', 0.3, {autoAlpha:0,stagger:0.08,pause:true,y:-50,ease: "Power4.out",durantion:'0.1'},'-=2')
            .to('.menuItems', {width: '0',durantion:'1', rotationZ: 0,
                force3D: false,maxWidth: '0',ease: "power2.out", opacity: 0},'-=0.5')
            .to('.menuItems .menuBar__hamburger', {autoAlpha: 0,ease: "power2.out"},'-=3')
            .to('.menuItems .menu_image ul li',0.3, {autoAlpha: 0,ease: "power2.out"},'-=5')
        .to('.menuItems',1, {display: 'none'})
    });


    if (window_width <= 992) {
        jQuery('.menuBar__hamburger').click(function () {
            jQuery('.menuForMobile').slideToggle();
            jQuery(this).toggleClass('close');

        });

        // jQuery('.menuForMobile .mobile_menu_list ul li.has-child').each(function (e){
        //     let i = jQuery(this).index();
        //     jQuery(this).not('.menuForMobile .mobile_menu_list ul li.has-child a').click(function (index) {
        //         jQuery('.submenu').not(jQuery(this).find('.submenu')).slideUp();
        //         jQuery(this).find('.submenu').slideToggle();
        //         index.stopPropagation();
        //         index.preventDefault();
        //     });
        //
        // });
        //


        jQuery(".menuForMobile .mobile_menu_list ul li.has-child").each(function (index) {
            jQuery(this).click(function (event) {
                jQuery('.submenu').eq(index).slideToggle();
                event.preventDefault();
                event.stopImmediatePropagation();

            });
            jQuery('.submenu').click(function (e) {
                e.stopPropagation();
                e.stopImmediatePropagation();
            })
        })


        //


    }

    if(window_width <= 767){
        jQuery('.child-here').hide();

        jQuery('.menuItems__wrap__left ul.mymenu li.has-child a').each(function(){
            jQuery(this).click(function(event){
                jQuery(this).siblings(".child-here").slideToggle();
                jQuery(this).closest(".has-child").siblings().find(".child-here").slideUp();
            });
        });
    }


    if(window_width > 767) {
        jQuery('.child-here li').css({'opacity':'1','transform':'translateX(-100px)'})
        gsap.utils.toArray(".mymenu .has-child").forEach(el => {
            // get just the nested <li> submenu items inside this one
            let items = el.querySelectorAll(".child-here li");
            // if any are found, create the animation and mouseover/mouseout listeners
            if (items.length > 0) {
                let animation = gsap.to(items, {opacity: 0, x: -100}, {
                    autoAlpha: 1,
                    display: "block",
                    stagger: 0,
                    x: 0,
                    paused: false
                });
                el.addEventListener("mouseover", () => animation.play());
                el.addEventListener("mouseout", () => animation.reverse());
            }
        });

    }





    // Sticky menu
    var screenPosition = 0;

    jQuery(window).scroll(function () {
        var scrolled = jQuery(window).scrollTop();
        //check if header-nav has navbar-fixed or not
        if (jQuery("body").hasClass("babylon_single")) {
        } else {
            if (screenPosition - scrolled > 0) {
                jQuery(".menuBar").css({"transform":"translateY(-120px)"});
                jQuery(".menuBar").removeClass("ShowIt");
                setTimeout(function () {
                    jQuery(".menuBar").css("background", "linear-gradient(180deg, rgba(0, 0, 0, 1) 0%, rgba(0, 0, 0, 0) 100%)");

                },700)
            } else {
                jQuery(".menuBar").css({"transform": "translateY(0px)", 'background': 'white'});
                jQuery(".menuBar").addClass("ShowIt");


            }
            screenPosition = scrolled;
            var first_section = jQuery("body").position().top + 200;
            jQuery(window).scroll(function () {
                if (jQuery(window).scrollTop() <= first_section) {
                    jQuery(".menuBar").css("transform", "translateY(0px)");
                    jQuery(".menuBar").css("background", "linear-gradient(180deg, rgba(0, 0, 0, 1) 0%, rgba(0, 0, 0, 0) 100%)");

                    jQuery(".menuBar").removeClass("ShowIt");

                }
            });

        }
    });









    gsap.registerPlugin(ScrollTrigger);

    let allAnim = document.querySelectorAll('.fade-up');
    allAnim.forEach((el, index) => {
        gsap.fromTo(el, {
            autoAlpha: 0,
            y: 50,
            ease: "none",
        }, {
            y: 0,
            autoAlpha: 1,
            ease: "power2",
            duration: 1,
            scrollTrigger: {
                id: `jQuery{index + 1}`,
                trigger: el,
                // start: 'top center+=100',
                toggleActions: 'play none none reverse',
            }
        })
    })


    gsap.registerPlugin(ScrollTrigger);

    let revealContainers = document.querySelectorAll(".reveal");
    const rule = CSSRulePlugin.getRule(".reveal img");
    revealContainers.forEach((container) => {
        let image = container.querySelector("img");
        let tl = gsap.timeline({
            scrollTrigger: {
                trigger: container,
                toggleActions: "play none none reset"
            }
        });

        tl.to("body", { duration: 0.5, autoAlpha: 1 })
            .to("img", { duration: 0.2, opacity: 1, delay: -1 })
            .to(".reveal img", {
                duration: 1,
                transform: 'translate(0,0)',
                ease: "Power2.out",
                width: '100%',
                height : '100%',
            });
    });

    // let revealContainers = document.querySelectorAll(".reveal");
    //
    // revealContainers.forEach((container) => {
    //     let image = container.querySelector("img");
    //     let tl = gsap.timeline({
    //         scrollTrigger: {
    //             trigger: container,
    //             toggleActions: "restart none none reset"
    //         }
    //     });
    //
    //     tl.set(container, { autoAlpha: 1 });
    //     tl.from(container, 1.5, {
    //         xPercent: '-100%',
    //         ease: Power2.out
    //     });
    //     tl.from(image, 1.5, {
    //         xPercent: '100%',
    //         scale: 1.3,
    //         delay: -1.5,
    //         ease: Power2.out
    //     });
    // });




    // gsap function
    function GLOBAL() {
        if (jQuery("#load") != null && jQuery("#load") != undefined) {
            var bc = jQuery("#load");
            var bcm = jQuery("#load img");
            gsap.to(bc, {
                opacity: 0,
                duration: 0.5,
                ease: "power2.out",
                onComplete: function() {
                    gsap.set(bc, {
                        display: "none"
                    })
                }
            }
            )
            gsap.to(bcm, {
                opacity: 0,
                duration: 0.5,
                ease: "power2.out",
                onComplete: function() {
                    gsap.set(bc, {
                        display: "none"
                    })
                }
            })

        }
    }

    jQuery(window).on("unload", function() {}, false);
    jQuery(window).on("load", function() {
        gsap.delayedCall(0.5, GLOBAL)
    });

})(jQuery);
